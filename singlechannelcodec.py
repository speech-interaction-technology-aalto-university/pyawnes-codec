# functional
import windowing
import numpy as np
import scipy
import torch 
import codingtools 


# config
import codec_config
import envelope


def logistic_statistics(L,R):
    cL = scipy.special.expit(L)
    cR = scipy.special.expit(R)
    cmu = 0.5*(cL+cR)*(1-2e-12) + 1e-12
    mu = scipy.special.logit(cmu)
    mu = np.where(mu < L, L, mu)
    mu = np.where(mu > R, R, mu)

    du = (cR-cL+1e-12)/(12**0.5)
    dxdu = 1/(cmu*(1-cmu) + 1e-12)
    dx = dxdu*du
    var = dx**2

    cRcL = cR-cL   

    return mu, var, cRcL


def cdf_lmm(y,mu,A,prop):    
    cc = scipy.special.expit(A*(y-mu))
    c = np.sum(cc*prop)
    return c

def binpdf_lmm(L,R,mu,A,prop):
    p_lmm = cdf_lmm(R,mu,A,prop) - cdf_lmm(L,mu,A,prop)
    return p_lmm


class single_channel_codec():
    def __init__(self,bits_per_second=16000,window_length_ms=30,window_step_ms=20):
        # initialize codec
        self.bits_per_second = bits_per_second
        self.bits_per_frame = int(bits_per_second * window_step_ms / 1000)
        self.window_step_ms = window_step_ms
        self.window_length_ms = window_length_ms
        self.fs = 16000        
        self.tf = windowing.mdct(window_length_ms,window_step_ms,self.fs,codec_config.dtype)
        self.env = envelope.LinearModel(codec_config,self.tf)
        self.envelope_coder = envelope.EnvelopeCoder(codec_config,self.env)


        self.bit_bias = np.load(codec_config.path_spectral_bitrate_bias)
        spectral_pdf = np.load(codec_config.path_spectral_pdf,allow_pickle=True)[()]

        self.propnp = spectral_pdf['proportion']
        self.propnp = np.divide(self.propnp, np.sum(self.propnp,axis=1,keepdims=True))
        self.Anp = np.abs(spectral_pdf['A'])
        self.iAnp = 1/self.Anp
        self.munp = spectral_pdf['mu']
        self.speclen = self.Anp.shape[0]
        self.selfspectral_mixture_models = spectral_pdf['A'].shape[1]
        
    def encode(self,original):
        self.spectrogram = self.tf.transform(original)
        
        frames = self.spectrogram.shape[1]
        framelist = range(frames)

        bits_left = (self.bits_per_frame*torch.ones(frames,dtype=codec_config.dtype)).detach().numpy()

        # Extract, quantize and encode envelope
        envelope_parameters = self.env.normalize_envelope_parameters(self.env.extract_parameters(self.spectrogram))
        self.bits_envelope, quantized_envelope_parameters = self.envelope_coder.encode(envelope_parameters)
        self.quantized_power_envelope = self.env.reconstruct_power_envelope(self.env.unnormalize_envelope_parameters(quantized_envelope_parameters))
        #self.quantized_smooth_envelope = self.env.reconstruct_smooth_envelope(self.env.unnormalize_envelope_parameters(quantized_envelope_parameters))

        white_spectrogram = self.env.whiten_spectrum(self.spectrogram,self.quantized_power_envelope)

        bits_left -= self.bits_envelope.detach().numpy()

        # perceptual model
        #self.perceptual_envelope = codingtools.perceptual_envelope_lpc(self.spectrogram,self.tf)
        self.perceptual_envelope = codingtools.perceptual_envelope_appr(quantized_envelope_parameters,self.tf)        
        #self.perceptual_envelope = codingtools.perceptual_envelope_lpc(self.quantized_smooth_envelope.detach().numpy(),self.tf)

        # Determine bitrate for each frequency component 
        bits_per_frequency = 0.5* self.quantized_power_envelope.log2() + self.perceptual_envelope.log2() + torch.tensor(self.bit_bias).unsqueeze(1).expand(self.speclen,frames)


        self.quantized_white_spectrogram = np.random.randn(self.speclen,frames)*0.5
        whitenoise_magnitude_spectrogram = np.zeros([self.speclen,frames])
        dither = torch.rand(self.speclen,dtype=codec_config.dtype)


        # sort bits and spectrum in ascending order of bits
        bits_sorted, indices = torch.sort(bits_per_frequency,dim=0)
        cumulative_bits = torch.cat((torch.zeros(1,frames,dtype=codec_config.dtype),bits_sorted)).cumsum(dim=0).detach().numpy()
        bits_sorted = bits_sorted.detach().numpy()
        offset = np.zeros(frames)
        L = np.zeros([self.speclen,frames])
        R = np.zeros([self.speclen,frames])
        #s2 = np.zeros([self.speclen,frames])
        for frameix in range(frames):            

            # find offset where a sum of non-negative bits matches target bitrate
            border_L = -1
            border_R = self.speclen-1
            while 1:
                mid = int((border_L+border_R+1)/2)
                offset[frameix] = (bits_left[frameix] - (cumulative_bits[-1,frameix]-cumulative_bits[mid,frameix]))/(self.speclen-mid)                
                s = int((bits_sorted[mid,frameix] + offset[frameix]) > 0)
                border_R = s*mid + (1-s)*border_R
                border_L = (1-s)*mid + s*border_L
                if np.max(border_R-border_L) <= 1:
                    break
            offset[frameix] = (bits_left[frameix] - (cumulative_bits[-1,frameix]-cumulative_bits[border_R,frameix]))/(self.speclen-border_R)                                    


        for freqix in range(self.speclen-1,-1,-1):
            freqix_index = indices[freqix,:]       

            bits_Q = bits_sorted[freqix,:] + offset - self.bit_bias[freqix_index]
            Q = 2**-bits_Q              

            L[freqix,:] = (((white_spectrogram[freqix_index,framelist]/Q + dither[freqix]).floor() - dither[freqix])*Q).detach().numpy()
            R[freqix,:] = L[freqix,:] + Q

            Lw = self.Anp[freqix_index,:]*(np.reshape(L[freqix,:],[frames,1])-self.munp[freqix_index,:])
            Rw = self.Anp[freqix_index,:]*(np.reshape(R[freqix,:],[frames,1])-self.munp[freqix_index,:])
            muwh, varwh, pwh = logistic_statistics(Lw,Rw)
            #pwh = pwh/(np.sum(pwh,axis=1,keepdims=True)+1e-12)
            #muw, pw = logistic_expectation(Lw,Rw)
            p = np.sum(pwh*self.propnp[freqix_index,:],axis=1)
            ph = pwh*self.propnp[freqix_index,:]
            varh = np.sum(np.divide(varwh,self.Anp[freqix_index,:]**2+1e-12)*ph,axis=1)
            stdh = 2*np.sqrt(varh)
            expectation = np.divide(muwh,self.Anp[freqix_index,:]+1e-12) + self.munp[freqix_index,:]        

            # calculate bitrate
            bits_freq = -np.log2(p+1e-12)
            bits_left -= bits_freq
            foo = (bits_left >0).astype(np.int) 

            self.quantized_white_spectrogram[freqix_index,framelist] = foo*np.sum(self.propnp[freqix_index,:]*expectation,axis=1) + self.quantized_white_spectrogram[freqix_index,framelist]*(1-foo)
            whitenoise_magnitude_spectrogram[freqix_index,framelist] = foo*stdh + (1-foo).astype(np.int)

            #s2[freqix_index,framelist] = foo*s2[freqix_index,framelist] + (1-foo)
            #if freqix > 0:
            #    offset += (bits_sorted[freqix,:]+offset-bits_freq)/freqix


        self.quantized_spectrogram = self.env.unwhiten_spectrum(torch.tensor(self.quantized_white_spectrogram),self.quantized_power_envelope)
        self.noise_magnitude_spectrogram = self.env.unwhiten_spectrum(torch.tensor(whitenoise_magnitude_spectrogram),self.quantized_power_envelope)

        quantized = self.tf.reconstruct_transform(self.quantized_spectrogram)
        return quantized
