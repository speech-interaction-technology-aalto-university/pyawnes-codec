#!/usr/bin/env python

import torch
import sys



dtype = torch.double
eps = sys.float_info.epsilon

path_modeldata = 'modeldata/'
path_extension_torch = '.pt'
path_extension_numpy = '.npy'
path_extension_numpyz = '.npz'
path_envelope_matrix            = path_modeldata + 'envelope_matrix'            + path_extension_numpy
path_inverse_envelope_matrix    = path_modeldata + 'inverse_envelope_matrix'    + path_extension_numpy
path_envelope_quantization_step = path_modeldata + 'envelope_quantization_step' + path_extension_numpy
path_envelope_normalization     = path_modeldata + 'envelope_normalization'     + path_extension_numpy
path_envelope_mixture_model     = path_modeldata + 'envelope_mixture_model'     + path_extension_numpyz
path_spectral_pdf               = path_modeldata + 'spectral_pdf'               + path_extension_numpy
path_spectral_bitrate_bias      = path_modeldata + 'spectral_bitrate_bias'      + path_extension_numpy
path_per_F0_spectral_energies   = path_modeldata + 'per_F0_spectral_energies'   + path_extension_numpy