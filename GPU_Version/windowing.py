import numpy as np
import torch
import transforms

class windowing(torch.nn.Module):
    
    def __init__(self,window_length_ms,window_step_ms,fs,dtype,device,window_function='halfsine'):
        super(windowing, self).__init__()
        self.device = device
        self.length = int(window_length_ms*fs/1000) 
        self.step = int(window_step_ms*fs/1000) 
        self.overlap = self.length - self.step
        self.dtype = dtype

        # global leftwin
        
        # if (self.length < 0) or (self.step < 0) or (self.overlap*2 > self.length):
        #     error('Window configuration error.')
        centerwin = np.ones(self.length - 2*self.overlap)

        if window_function == 'halfsine':
            leftwin = np.sin(np.pi*np.linspace(0.5,self.overlap-0.5,num=self.overlap)/(2*self.overlap)) # half-sine window
        # else:
        #     error('Unknown windowing function: ' + windef['function'])

        rightwin = np.flipud(leftwin)
        self.function = np.concatenate((leftwin,centerwin,rightwin))
        self.function = torch.tensor(np.reshape(self.function,[self.length,1]),device=self.device)
        
    def window_count(self,data):        
        return 1+int((len(data)-self.length)/self.step)

    def extract(self,data):
        wincnt = self.window_count(data)
        truncate_to_length = (wincnt-1)*self.step + self.length
        data = data[0:truncate_to_length]
        datamatrix = torch.reshape(torch.cat((data,torch.zeros([self.length-2*self.overlap,1],dtype=self.dtype,device=self.device))),[wincnt+1,self.length-self.overlap]).t()
        overlapmatrix = datamatrix[0:self.overlap,1:wincnt+1];
        datamatrix = torch.cat((datamatrix[:,0:wincnt],overlapmatrix))
        
        return torch.mul(datamatrix, self.function) # windowing function
    
    def reconstruct(self,datamatrix):
        wincnt = datamatrix.shape[1]
        data_length = (wincnt-1)*self.step + self.length
        
        datamatrix = torch.mul(datamatrix, self.function) # windowing function        
        overlap = datamatrix[(self.length-self.overlap):self.length,0:wincnt-1]
        datamatrix[0:self.overlap,1:wincnt] += overlap # overlap add
        foo = torch.reshape(datamatrix[0:(self.length-self.overlap),:].t(),[data_length-self.overlap,1])
        bar = datamatrix[(self.length-self.overlap):self.length,(wincnt-1):wincnt]
        data = torch.cat((foo,bar))
        return data

    

    
class mdct(windowing):
        
    def transform(self,data):
        if data.shape[0] != self.length:
            # apply windowing
            data = self.extract(data).type(self.dtype)
        spectrogram = torch.zeros([self.step, data.shape[1]],dtype=self.dtype,device=self.device)
        padding = torch.zeros([int(self.step-self.length/2),data.shape[1]],dtype=self.dtype,device=self.device)
        data = torch.cat((padding,data,padding))
        for k in range(data.shape[1]):
            spectrogram[:,k] = transforms.mdct(data[:,k]).squeeze(1)
        return spectrogram
    
    def inverse_transform(self,spectrogram):
        data = torch.zeros([self.step*2,spectrogram.shape[1]],dtype=self.dtype,device=self.device)
        for k in range(spectrogram.shape[1]):
            data[:,k] = transforms.imdct(spectrogram[:,k]).squeeze(1)
        data = data[int(self.step-self.length/2):int(self.step+self.length/2),:]
        return data

    def reconstruct_transform(self,spectrogram):
        return self.reconstruct(self.inverse_transform(spectrogram))