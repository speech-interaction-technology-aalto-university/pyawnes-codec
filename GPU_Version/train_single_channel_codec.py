# functional
import windowing
import numpy as np
import torch 
import configparser
import training_models
import os
import codingtools
import envelope


# To train all even if models already exist, set to 'True'
force_training = True
force_training_spectral_pdf = False

# dev
import matplotlib.pyplot as plt

# config
import codec_config

# dataset
import corpus

# initialize 
dataset = corpus.LibriSpeech(dtype=codec_config.dtype,mode='train')
params = {'batch_size': 1,
          'shuffle': True,
          'num_workers': 1}
data_generator = torch.utils.data.DataLoader(dataset, **params)
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
print('device:',device)

# codec config
config = configparser.ConfigParser()
config.read('codec_config.ini')
fs = int(config['sampling']['frequency'])
window_length_ms = int(config['window']['length_ms'])
window_step_ms = int(config['window']['step_ms'])
tf = windowing.mdct(window_length_ms,window_step_ms,fs,codec_config.dtype,device)
tf.to(device)
envelope_order = int(config['envelope']['order'])


if force_training | (os.path.isfile(codec_config.path_envelope_matrix) == False):
    print('Training of envelope model and spectral pdf')

    # training setup
    spectrum_length = tf.step
    spectrogram_model = training_models.LogisticMixture(spectrum_length,5,codec_config.dtype, device)

    # Entropy Coder Envelope Model
    envelope_model = training_models.EnvelopeModel_Entropy(spectrum_length, envelope_order, codec_config.dtype, device,eps=codec_config.eps)

    # Vector Quantization Envelope Model
    # num_qbits = [10, 10, 10, 10]
    # envelope_model = training_models.EnvelopeModel_VQ(spectrum_length, envelope_order, num_qbits, codec_config.dtype, device, eps=codec_config.eps)

    # pre-train for MSVQ
    # for batch in data_generator:
    #     spectrogram = tf.transform(batch[0].to(device))
    #     envelope_model.initialize(spectrogram)
    #     break


    # train
    learning_rate = 1e-2
    optimizer = torch.optim.Adam(list(spectrogram_model.parameters()) + list(envelope_model.parameters()), lr=learning_rate)

    alpha = 0.01
    batchcounter = 0
    for epoch in range(1):
        for batch in data_generator:

            spectrogram = tf.transform(batch[0].to(device))

            # quantize envelope parameters with Entropy Coder method
            _, magnitude_envelope, negloglike_envelope = envelope_model.forward(spectrogram)
            negloglike_spectrogram = spectrogram_model(spectrogram.t(), magnitude_envelope.t())
            loss = negloglike_envelope + negloglike_spectrogram

            # quantize envelope parameters with MSVQ
            # _, magnitude_envelope = envelope_model.forward(spectrogram)
            # negloglike_spectrogram = spectrogram_model(spectrogram.t(), magnitude_envelope.t())
            # loss = negloglike_spectrogram


            optimizer.zero_grad()
            loss.backward()
            optimizer.step()


            batchcounter += 1

            # codebooks evaluation for VQ method
            # if ((batchcounter % 100 == 0) & (batchcounter < 1000)) | ((batchcounter % 1000 == 0) & (batchcounter < 10000)) :
            #     envelope_model.replace_unused()

            if batchcounter % 100 == 0:

                print('Batch ' + str(batchcounter) + ', Loss: ' + str(loss.item()))
                plt.subplot(211)
                plt.plot(envelope_model.forward_transform().t().cpu().detach().numpy())
                plt.subplot(212)
                plt.plot(envelope_model.backward_transform().cpu().detach().numpy())
                plt.subplot(313)
                plt.plot(envelope_model.quantization_step.cpu().detach().numpy())
                plt.show()
                frameix=30
                plt.plot(20*spectrogram[:,frameix].abs().log10().cpu().detach().numpy())
                plt.plot(20*magnitude_envelope[:,frameix].abs().log10().cpu().detach().numpy())
                plt.show()
            if batchcounter == 10000:
                np.save(codec_config.path_envelope_matrix,envelope_model.forward_transform().cpu().detach().numpy())
                np.save(codec_config.path_inverse_envelope_matrix,envelope_model.backward_transform().cpu().detach().numpy())
                np.save(codec_config.path_envelope_quantization_step, envelope_model.quantization_step.squeeze().cpu().detach().numpy())

                prop, A, mu = spectrogram_model.extract_parameters()
                spectral_pdf = {
                    "proportion": prop.squeeze().cpu().detach().numpy(),
                    "A": A.squeeze().cpu().detach().numpy(),
                    "mu": mu.squeeze().cpu().detach().numpy()
                    }
                np.save(codec_config.path_spectral_pdf,spectral_pdf)


        np.save(codec_config.path_envelope_matrix,envelope_model.forward_transform().cpu().detach().numpy())
        np.save(codec_config.path_inverse_envelope_matrix,envelope_model.backward_transform().cpu().detach().numpy())
        # np.save(codec_config.path_envelope_vq,envelope_model.codebooks.cpu().detach().numpy())
        np.save(codec_config.path_envelope_quantization_step, envelope_model.quantization_step.squeeze().cpu().detach().numpy())
        prop, A, mu = spectrogram_model.extract_parameters()
        spectral_pdf = {
            "proportion": prop.squeeze().cpu().detach().numpy(),
            "A": A.squeeze().cpu().detach().numpy(),
            "mu": mu.squeeze().cpu().detach().numpy()
            }
        np.save(codec_config.path_spectral_pdf,spectral_pdf)
    print('Training of envelope model and spectral pdf finished successfully...!!!')


if force_training | (os.path.isfile(codec_config.path_envelope_normalization) == False):
    print('Training of entropy model and normalization for envelope (mean and variance)')

    if 0:
        # training setup
        spectrum_length = tf.step
        envelope_model = training_models.EnvelopeModel(spectrum_length, envelope_order, codec_config.dtype,device,eps=codec_config.eps)


        envelope_parameters_dB_mean = torch.zeros(envelope_order,dtype=codec_config.dtype, device=device)
        envelope_parameters_dB_squared = torch.zeros(envelope_order,dtype=codec_config.dtype, device=device)


        framecounter = 0
        batchcounter = 0
        for batch in data_generator:


            spectrogram = tf.transform(batch[0].to(device))
            frames = spectrogram.shape[1]

            envelope_parameters_dB, _, _ = envelope_model.forward(spectrogram)

            envelope_parameters_dB_mean += envelope_parameters_dB.sum(dim=1).detach()
            envelope_parameters_dB_squared += envelope_parameters_dB.square().sum(dim=1).detach()
            framecounter += frames

            batchcounter += 1
            if batchcounter % 100 == 0:
                print('Batch ' + str(batchcounter))

        envelope_parameters_dB_mean /= framecounter
        envelope_parameters_dB_squared /= framecounter

        envelope_normalization = {
            "mean": envelope_parameters_dB_mean,
            "variance": envelope_parameters_dB_squared - envelope_parameters_dB_mean.square()
            }

        np.save(codec_config.path_envelope_normalization,envelope_normalization)
        print('Training of entropy model and normalization finished successfully...!!!')

    else:

        # training setup
        spectrum_length = tf.step
        env = envelope.LinearModel(codec_config,tf,device)

        envelope_parameters_dB_mean = torch.zeros(envelope_order,dtype=codec_config.dtype, device=device)
        envelope_parameters_dB_squared = torch.zeros(envelope_order,dtype=codec_config.dtype, device=device)


        framecounter = 0
        batchcounter = 0
        for batch in data_generator:


            spectrogram = tf.transform(batch[0].to(device))
            frames = spectrogram.shape[1]

            envelope_parameters = env.extract_parameters(spectrogram)
            envelope_parameters_dB = codingtools.power_to_dB(envelope_parameters)

            envelope_parameters_dB_mean += envelope_parameters_dB.sum(dim=1).detach()

            envelope_parameters_dB_squared += envelope_parameters_dB.square().sum(dim=1).detach()
            framecounter += frames

            batchcounter += 1
            if batchcounter % 100 == 0:
                print('Batch ' + str(batchcounter))

        envelope_parameters_dB_mean /= framecounter
        envelope_parameters_dB_squared /= framecounter

        envelope_normalization = {
            "mean": envelope_parameters_dB_mean,
            "variance": envelope_parameters_dB_squared - envelope_parameters_dB_mean.square()
            }

        np.save(codec_config.path_envelope_normalization,envelope_normalization)
        print('Training of entropy model and normalization finished successfully...!!!')
        

if force_training | force_training_spectral_pdf | (os.path.isfile(codec_config.path_spectral_bitrate_bias) == False):
    print('Training of entropy bias for spectral components (logistic mixture model)')
    
    # initialize codec
    env = envelope.LinearModel(codec_config,tf,device)
        
    spectral_pdf = np.load(codec_config.path_spectral_pdf,allow_pickle=True)[()]
    
    A = torch.tensor(spectral_pdf['A'],dtype=codec_config.dtype, device=device)
    mu = torch.tensor(spectral_pdf['mu'],dtype=codec_config.dtype, device=device)
    prop = torch.tensor(spectral_pdf['proportion'],dtype=codec_config.dtype, device=device)
    prop /= (prop.sum(dim=1,keepdims=True))
    
    speclen = A.shape[0]
    spectral_mixture_models = spectral_pdf['A'].shape[1]
    
    Q = 2**-8
    bits_Q = -np.log2(Q)
    
    bits = torch.zeros(speclen,dtype=codec_config.dtype, device=device)
    framecnt = 0
    batchcnt = 0
    batchcnt1pct = int(dataset.filecount()/100)
    
    for batch in data_generator:
        spectrogram = tf.transform(batch[0].to(device))
        batchcnt += 1
        print(batchcnt)
        frames = spectrogram.shape[1]
    
        envelope_parameters = env.normalize_envelope_parameters(env.extract_parameters(spectrogram))
    
        quantized_envelope_parameters = codingtools.quantizer(envelope_parameters,env.ndq.unsqueeze(1).expand(env.order,frames))
        quantized_power_envelope = env.reconstruct_power_envelope(env.unnormalize_envelope_parameters(quantized_envelope_parameters))
    
        white_spectrogram = env.whiten_spectrum(spectrogram,quantized_power_envelope)
        
        dither = torch.rand(frames,dtype=codec_config.dtype, device=device)
        
        framecnt += frames

        for k in range(speclen):
            _,bk = codingtools.quantize_lmm(white_spectrogram[k,:],Q,mu[k,:],A[k,:],prop[k,:])
            bits[k] += bk.sum()
    
        if batchcnt % batchcnt1pct == 0:
            plt.plot((bits/framecnt - bits_Q).cpu().detach().numpy())
            plt.title(str(int(batchcnt / batchcnt1pct)) + '%')
            plt.show()
        
    bit_bias = (bits/framecnt - bits_Q)
    np.save(codec_config.path_spectral_bitrate_bias,bit_bias.cpu().detach().numpy())
    print('Training of entropy bias finished successfully...!!!')