import torch
import numpy as np
import scipy
from scipy import signal
import os

def power_to_dB(x):
    return x.log10().mul(10.)

def dB_to_power(x):
    return torch.pow(10.,x.div(10))

def quantizer(x,dq):
    return x.div(dq).round().mul(dq)

def quantization_bin(x,dq):
    xdq = x.div(dq)
    L = xdq.floor()
    R = L+1
    return L.mul(dq), R.mul(dq)


def quantizer_simulator(x,dq):
    return x.add(dq*(0.5-torch.rand(x.shape)))

def quantizer_simulator_full(x,dq):
    return x.add((0.5-torch.rand(x.shape)).mul(dq))

def normcdf(x):
    return x.mul(np.pi/np.sqrt(3.)).sigmoid()

def bit_offset(bits_per_frequency,bits_left):
    speclen = bits_per_frequency.shape[0]
    bits_sorted, indices = torch.sort(bits_per_frequency)
    borders = [-1,speclen-1]
    cumulative_bits = torch.cat((torch.zeros(1),bits_sorted)).cumsum(dim=0)
    while 1:
        mid = int((borders[0]+borders[1]+1)/2)
        #print(str(borders) + ' ' + str(mid))
        offset = (bits_left - (cumulative_bits[-1]-cumulative_bits[mid]))/(speclen-mid)                
        if (bits_sorted[mid] + offset) > 0:
            borders[1] = mid
        else:
            borders[0] = mid
        if (borders[1]-borders[0]) <= 1:
            break
    offset = (bits_left - (cumulative_bits[-1]-cumulative_bits[borders[1]]))/(speclen-borders[1])                                    
    return offset, indices


def int_t2fh(t):
    a = -np.sqrt(16/3)
    b = np.sqrt(3/4)
    return (-(np.pi**2)/3)*((1 + (b*t).exp()*(1+b*t))/((1+(b*t).exp())**2) - 1)

def logistic_statistics(L,R):
    cR = R.sigmoid()
    cL = L.sigmoid()
    sp = torch.nn.Softplus()
    sL = sp(L)
    sR = sp(L)
    Z = (cR - cL + 1e-12)
    mu1 = (R*cR - sR - L*cL + sL)/Z   
    var1 = (int_t2fh(R)-int_t2fh(L))/Z  - mu**2
    mu2 = (R+L)/2
    var2 = (R-L).abs()**2/12
    d = (R > -1.5) & (L < 1.5)
    mu = d*mu1 + (1-d)*mu2
    var = d*var1 + (1-d)*var2
    return mu, var
    

def logistic_expectation(L,R):    
    cL = L.sigmoid()
    cR = R.sigmoid()
    if 0:
        sp = torch.nn.Softplus()
        sL = sp(L)
        sR = sp(L)
        #sL = (L.exp() +1).log()
        #sR = (R.exp() +1).log()

        mu = (R*cR - sR - L*cL + sL)/(cR - cL + 1e-6)
    else:
        a = -4.*np.log(2.)
        b = np.power(2.*np.log(2),-0.5)
        cbL = (1+(-L*b).exp()).pow(-1)
        afbL = a*cbL.mul(1-cbL)        
        cbR = (1+(-R*b).exp()).pow(-1)
        afbR = a*cbR.mul(1-cbR)        
        mu = (afbR-afbL).div(cR - cL + 1e-6)
    return mu

def logistic_variance(L,R):
    s = torch.nn.Softplus()
    def c(x): return torch.sigmoid(x)
    #def f(x): 
    #    cx = c(x)
    #    return cx.mul(1-cx)
    def df(x): 
        cx = c(x)
        return cx - 3*cx.pow(2) + 2*cx.pow(3)
    def df_cx(cx):
        return cx - 3*cx.pow(2) + 2*cx.pow(3)

    alpha0 = 2*(np.pi**2)/3 
    alpha1 = (4/(np.pi**2))**(1/3)
    alpha2 = (np.pi**2)/3  

    cL = c(L)
    cR = c(R)
    sL = s(L)
    sR = s(R)

    mu = (R*cR - sR - L*cL + sL)/(cR - cL + 1e-12)
    calpha1L = c(alpha1*L)
    calpha1R = c(alpha1*R)
    s2 = (-mu.pow(2).mul(cR - cL) + alpha0*df_cx(calpha1R) + alpha2*calpha1R - alpha0*df_cx(calpha1L) - alpha2*calpha1L).div(cR-cL)
    return s2

def cdf_lmm(y,mu,A,prop):    
    cc = torch.sigmoid(A.unsqueeze(1)*(y.unsqueeze(0)-mu.unsqueeze(1)))
    c = cc.mul(prop.unsqueeze(1)).sum(dim=0,keepdims=True)
    return c

def binpdf_lmm(L,R,mu,A,prop):
    p_lmm = (cdf_lmm(R,mu,A,prop) - cdf_lmm(L,mu,A,prop)).squeeze()
    return p_lmm

def pdf_lmm(y,mu,A,prop):
    cc = torch.sigmoid(A*(y.unsqueeze(0)-mu))
    c = cc.mul(prop).sum(dim=0,keepdims=True)
    fc = A*cc*(1-cc)
    f = fc.mul(prop).sum(dim=0,keepdims=True)
    return f

def invsig(x):
    return ((x+1e-12).pow(-1)-1.).log()

def generate_lmm(N,mu,A,prop):
    M = prop.shape[0]
    p = torch.rand(N)
    cprop = torch.cat((torch.zeros(1,1),torch.cumsum(prop,0)),0)
    q = (p > cprop[0:M]) & (p < cprop[1:M+1])
    xh = invsig(torch.rand(M,N)).div(A)+mu
    x = (xh*q).sum(dim=0)
    return x

def quantize_lmm(x,Q,mu,A,prop):
    device = x.device
    spectral_mixture_models = prop.shape[0]
    frames = x.shape[0]
    xq = torch.zeros(frames,dtype=x.dtype, device=device)
    bits = torch.zeros(frames,dtype=x.dtype, device=device)
    dither = np.random.rand(frames)
    L = ((x.cpu() + dither).floor() - dither)*Q
    L = L.to(device)
    R = L + Q
    for m in range(spectral_mixture_models):
            Lw = A[m]*(L-mu[m])
            Rw = A[m]*(R-mu[m])
            expectation = logistic_expectation(Lw,Rw)/A[m] + mu[m]
            xq += prop[m]*expectation
    p_lmm = binpdf_lmm(L,R,mu,A[:],prop)
    bits -= p_lmm.log2()
    return xq,bits

def perceptual_envelope_lpc(quantized_power_envelope,tf):
    frames = quantized_power_envelope.shape[1]
    speclen = quantized_power_envelope.shape[0]
    
    # pre-emphasis
    w = np.zeros([2*speclen+1,1],dtype=np.float64)
    w[0] = 1
    w[1] = -.68
    W = np.abs(scipy.fft.rfft(w,axis=0)**2)
    W = W[0:speclen,:].real

    # autocorrelation
    R = scipy.fft.irfft(quantized_power_envelope*W,axis=0)
    R = R.real
    R[0] *= 1.01


    
    b = np.zeros(17)
    b[0] = 1
    w = 0.92**np.linspace(0,16,17)
    
    W = torch.zeros(speclen,frames)
    for k in range(frames):
        if R[0,k] != 0:
            a = solve_toeplitz(R[0:17,k],b)
            a /= a[0]
            a *= w
            foo = scipy.fft.ifft(a,axis=0,n=2*speclen)
            W[:,k] = torch.tensor(foo[0:speclen]).abs()
        else:
            W[:,k] = torch.ones(speclen,dtype=spectrogram.dtype)
        
    return W

def perceptual_envelope_lpc_spectrogram(spectrogram,tf):
    frames = spectrogram.shape[1]
    speclen = spectrogram.shape[0]
    
    wins = tf.inverse_transform(spectrogram)
    wins *= tf.function
    wins_preemph = signal.lfilter([1,-.68],1,wins.detach().numpy(),axis=0)
    R = scipy.fft.ifft(np.abs(scipy.fft.fft(wins_preemph,axis=0))**2,axis=0)
    R = R.real
    R[0] *= 1.01

    
    b = np.zeros(17)
    b[0] = 1
    w = 0.92**np.linspace(0,16,17)
    
    W = torch.zeros(speclen,frames)
    for k in range(frames):
        if R[0,k] != 0:
            a = solve_toeplitz(R[0:17,k],b)
            a /= a[0]
            a *= w
            foo = scipy.fft.ifft(a,axis=0,n=2*speclen)
            W[:,k] = torch.tensor(foo[0:speclen]).abs()
        else:
            W[:,k] = torch.ones(speclen,dtype=spectrogram.dtype)
        
    return W

if os.path.isfile('modeldata/weights_full.npz'):
    param = np.load('modeldata/weights_full.npz', allow_pickle='True')
else:
    warning('Perceptual model has not yet been trained.')

def perceptual_envelope_appr(envelope_param, tf):
    frames = envelope_param.shape[1]
    envlen = envelope_param.shape[0]
    speclen = tf.step; 
    envelope_param=np.array(envelope_param)
    W = np.zeros([speclen,frames])
    for k in range(frames):
        pe = np.maximum(np.maximum(envelope_param[:,k]@param['w1'] +param['b1'],0)@param['w2']+param['b2'],0)@param['w3']+param['b3'];
        W[:,k]=10**(pe/10);
    W = np.maximum(W,1e-16)
    return (torch.tensor(W))