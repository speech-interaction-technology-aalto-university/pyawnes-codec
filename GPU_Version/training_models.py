import torch
import numpy as np
import codec_config
import codingtools
import torch.nn.functional as F

class LogisticMixture(torch.nn.Module):
    def __init__(self, input_dimensions, mixture_models, dtype, device):
        """
        Create model
        """
        super(LogisticMixture, self).__init__()
        self.device = device
        self.input_dimensions = input_dimensions
        self.mixture_models = mixture_models
        self.A = torch.nn.Parameter(torch.rand(input_dimensions,mixture_models,dtype=dtype, device=self.device))
        self.mu = torch.nn.Parameter(torch.randn(input_dimensions,mixture_models,dtype=dtype, device=self.device))
        self.prop0 = torch.nn.Parameter(torch.rand(input_dimensions,mixture_models,dtype=dtype, device=self.device))

    def forward(self, x, scaling):
        """
        Forward pass
        """
        # shape (frames,input_dimensions,mixture_models)
        frames = x.shape[0]
        thisshape = [frames,self.input_dimensions,self.mixture_models]
        invscaling = (scaling.abs() + 1e-12).pow(-1)

        # Normalize component proportion to unity
        prop = self.prop0.abs() # must be positive
        prop /= prop.sum(dim=-1,keepdims=True) + 1e-12 # normalize

        # Normalize component scaling to unit variance
        component_variance = self.A.pow(-2)*(np.pi**2)/3
        joint_mean = (prop*self.mu).sum(dim=-1,keepdims=True)
        joint_variance = (prop*(component_variance + self.mu**2)).sum(dim=-1,keepdims=True) - joint_mean**2
        A = self.A*((joint_variance*3/(np.pi**2)).sqrt().expand([self.input_dimensions,self.mixture_models]))

        # normalize to zero mean and unit variance
        xx = (x*invscaling).unsqueeze(-1).expand(thisshape) # expand with mixture dimensions
        xw = (xx-self.mu.unsqueeze(0).expand(thisshape))*A.unsqueeze(0).expand(thisshape) # normalize mean and variance

        # component probabilities
        cx = torch.sigmoid(xw)
        fx_component = (cx*(1-cx)*A.abs().unsqueeze(0).expand(thisshape))*invscaling.unsqueeze(-1).expand(thisshape)

        # joint probability
        fx = ( fx_component*prop.unsqueeze(0).expand(thisshape) ). sum(dim=-1) + 1e-12

        # negative log likelihood averaged frames
        return -fx.log().sum()/frames
    
    def extract_parameters(self):
        # Normalize component proportion to unity
        prop = self.prop0.abs() # must be positive
        prop /= prop.sum(dim=-1,keepdims=True) + 1e-12 # normalize

        # Normalize component scaling to unit variance
        component_variance = self.A.pow(-2)*(np.pi**2)/3
        joint_mean = (prop*self.mu).sum(dim=-1,keepdims=True)
        joint_variance = (prop*(component_variance + self.mu**2)).sum(dim=-1,keepdims=True) - joint_mean**2
        A = self.A*((joint_variance*3/(np.pi**2)).sqrt().expand([self.input_dimensions,self.mixture_models]))
      
        return prop, A, self.mu

class EnvelopeModel_Entropy(torch.nn.Module):
    def __init__(self, input_dimensions, envelope_dimensions, dtype,device, eps=1e-12):
        """
        Create model
        """
        super(EnvelopeModel_Entropy, self).__init__()
        self.input_dimensions = input_dimensions
        self.envelope_dimensions = envelope_dimensions
        self.eps = eps
        self.device = device

        envelope0 = torch.zeros(envelope_dimensions,input_dimensions,dtype=dtype,device=self.device)
        envbandwidth = int(input_dimensions/envelope_dimensions)
        for k in range(envelope_dimensions-1):
            envelope0[k,range(k*envbandwidth,(k+1)*envbandwidth)]=1
        envelope0[envelope_dimensions-1,(envelope_dimensions-1)*envbandwidth:]=1

        self.forward_transform_matrix = torch.nn.Parameter(envelope0)
        self.backward_transform_matrix = torch.nn.Parameter(torch.pinverse(envelope0).clone().detach())
        self.quantization_step = torch.nn.Parameter(torch.ones(envelope_dimensions,1,dtype=codec_config.dtype,device=self.device))
        #self.normalization = torch.nn.Parameter(torch.ones(envelope_dimensions,1,dtype=codec_config.dtype))

    def forward_transform(self):
        forward_transform = self.forward_transform_matrix.abs() # non-negative
        forward_transform = forward_transform * (forward_transform.sum(dim=1,keepdims=True) + self.eps).pow(-1).expand([self.envelope_dimensions,self.input_dimensions]) # normalize
        return forward_transform

    def backward_transform(self):
        backward_transform = self.backward_transform_matrix.abs() # non-negative
        #backward_transform = backward_transform * (backward_transform.sum(dim=1,keepdims=True) + self.eps).pow(-1).expand([self.input_dimensions,self.envelope_dimensions]) # normalize
        #transform_gain = (self.forward_transform() * backward_transform.t()).sum(dim=0,keepdims=True)
        #backward_transform = backward_transform * transform_gain.pow(-1).expand([self.envelope_dimensions,self.input_dimensions]).t()
        return backward_transform

    def forward(self, spectrogram):
        # spectrogram shape (input_dimensions,frames)
        frames = spectrogram.shape[1]

        # apply transform, output shape (envorder,frames), parameters in decibel
        envelope_parameters_dB = codingtools.power_to_dB(torch.matmul(self.forward_transform(),spectrogram.abs().square() + self.eps))

        # quantize
        quantization_noise = self.quantization_step.expand([self.envelope_dimensions,frames])*(torch.rand(self.envelope_dimensions,frames,device=self.device)-0.5)
        quantized_envelope_parameters_dB = envelope_parameters_dB + quantization_noise

        # backward transform, output shape (frames, input_dimensions)
        quantized_envelope_power = torch.matmul(self.backward_transform(),codingtools.dB_to_power(quantized_envelope_parameters_dB))
        quantized_envelope_magnitude = quantized_envelope_power.sqrt()

        # whiten/normalize spectrum (obsolete)
        #white_spectrogram = spectrogram*(quantized_envelope_power.pow(-2))

        # negative log2 likelihood is locally relative to the bitrate required to encode envelope
        loss = -((self.quantization_step.abs() + self.eps).log2().sum())

        return envelope_parameters_dB, quantized_envelope_magnitude, loss


class EnvelopeModel_VQ(torch.nn.Module):
    def __init__(self, input_dimensions, envelope_dimensions, num_qbits, dtype,device, eps=1e-12):
        """
        Create model
        """
        super(EnvelopeModel_VQ, self).__init__()
        self.input_dimensions = input_dimensions
        self.envelope_dimensions = envelope_dimensions
        self.device = device

        self.num_qbits = num_qbits
        self.num_stages = len(num_qbits)
        self.num_codebooks = [2 ** x for x in num_qbits]
        self.total_num_codebooks = int(np.sum(self.num_codebooks, axis=0))
        self.eps = eps
        self.dtype = dtype
        
        envelope0 = torch.zeros(envelope_dimensions,input_dimensions,dtype=dtype,device=self.device)
        envbandwidth = int(input_dimensions/envelope_dimensions)
        for k in range(envelope_dimensions-1):
            envelope0[k,range(k*envbandwidth,(k+1)*envbandwidth)]=1
        envelope0[envelope_dimensions-1,(envelope_dimensions-1)*envbandwidth:]=1

        self.codebooks = torch.nn.Parameter(torch.randn(self.total_num_codebooks,self.envelope_dimensions,dtype=dtype,device=self.device))
        self.forward_transform_matrix = torch.nn.Parameter(envelope0)
        self.backward_transform_matrix = torch.nn.Parameter(torch.pinverse(envelope0).clone().detach())
        self.codebooks_used = torch.zeros(self.num_codebooks[0],dtype=torch.bool, device=self.device)
        self.codebooks_used[:] = False
        

    def forward_transform(self):
        forward_transform = self.forward_transform_matrix.abs() # non-negative
        forward_transform = forward_transform * (forward_transform.sum(dim=1,keepdims=True) + self.eps).pow(-1).expand([self.envelope_dimensions,self.input_dimensions]) # normalize
        return forward_transform

    def backward_transform(self):
        backward_transform = self.backward_transform_matrix.abs() # non-negative        
        #backward_transform = backward_transform * (backward_transform.sum(dim=1,keepdims=True) + self.eps).pow(-1).expand([self.input_dimensions,self.envelope_dimensions]) # normalize
        #transform_gain = (self.forward_transform() * backward_transform.t()).sum(dim=0,keepdims=True)
        #backward_transform = backward_transform * transform_gain.pow(-1).expand([self.envelope_dimensions,self.input_dimensions]).t()
        return backward_transform

    def forward(self, spectrogram):
        # spectrogram shape (input_dimensions,frames)

        # apply transform, output shape (envorder,frames), parameters in decibel
        envelope_parameters_dB = codingtools.power_to_dB(torch.matmul(self.forward_transform(),spectrogram.abs().square() + self.eps))
        
        # quantize 
        # distance shape (codebook_entries,envelope_dimensions,frames)
        idx = 0

        distance0 =  (envelope_parameters_dB.unsqueeze(0) - self.codebooks[idx:idx+self.num_codebooks[0],:].unsqueeze(-1)).square().sum(dim=1)
        softmin_distance0 = F.softmin(distance0,dim=0).t()
        input_quantized0 = torch.matmul(softmin_distance0 ,self.codebooks[idx:idx+self.num_codebooks[0],:]).t()
        residual0 = envelope_parameters_dB - input_quantized0

        idx = idx + self.num_codebooks[0]

        distance1 = (residual0.unsqueeze(0) - self.codebooks[idx:idx + self.num_codebooks[1], :].unsqueeze(-1)).square().sum(dim=1)
        softmin_distance1 = F.softmin(distance1, dim=0).t()
        input_quantized1 = torch.matmul(softmin_distance1, self.codebooks[idx:idx + self.num_codebooks[1], :]).t()
        residual1 = residual0 - input_quantized1

        idx = idx + self.num_codebooks[1]

        distance2 = (residual1.unsqueeze(0) - self.codebooks[idx:idx + self.num_codebooks[2], :].unsqueeze(-1)).square().sum(dim=1)
        softmin_distance2 = F.softmin(distance2, dim=0).t()
        input_quantized2 = torch.matmul(softmin_distance2, self.codebooks[idx:idx + self.num_codebooks[2], :]).t()
        residual2 = residual1 - input_quantized2

        idx = idx + self.num_codebooks[2]

        distance3 = (residual2.unsqueeze(0) - self.codebooks[idx:idx + self.num_codebooks[3], :].unsqueeze(-1)).square().sum(dim=1)
        softmin_distance3 = F.softmin(distance3, dim=0).t()
        input_quantized3 = torch.matmul(softmin_distance3, self.codebooks[idx:idx + self.num_codebooks[3], :]).t()
        # residual3 = residual2 - input_quantized3
        #
        # idx = idx + self.num_codebooks[3]
        #
        # distance4 = (residual3.unsqueeze(0) - self.codebooks[idx:idx + self.num_codebooks[4], :].unsqueeze(-1)).square().sum(dim=1)
        # softmin_distance4 = F.softmin(distance4, dim=0).t()
        # input_quantized4 = torch.matmul(softmin_distance4, self.codebooks[idx:idx + self.num_codebooks[4], :]).t()
        # residual4 = residual3 - input_quantized4

        quantized_envelope_parameters_dB = input_quantized0 + input_quantized1 + input_quantized2 + input_quantized3 # + input_quantized4

        # monitor codebook use
        with torch.no_grad():

            self.best_codebook_indices = distance0.argmin(dim=0).clone()
            self.codebooks_used[self.best_codebook_indices] = True
            self.unused_indices = torch.where(self.codebooks_used.cpu() == False)[0]
            self.codebooks_used[:] = False

        
        # backward transform, output shape (frames, input_dimensions)
        quantized_envelope_power = torch.matmul(self.backward_transform(),codingtools.dB_to_power(quantized_envelope_parameters_dB))
        quantized_envelope_magnitude = quantized_envelope_power.sqrt()

        return envelope_parameters_dB, quantized_envelope_magnitude

    def replace(self,new_codebook):
        with torch.no_grad():
            new_length = min([self.num_codebooks[0],new_codebook.shape[1]])
            self.codebooks[range(new_length)] = new_codebook[:,range(new_length)].t().clone()
            # self.codebooks_used[range(new_length)] = True
            # self.replace_unused()
            
    def initialize(self,spectrogram):
        # apply transform, output shape (envorder,frames), parameters in decibel
        envelope_parameters_dB = codingtools.power_to_dB(torch.matmul(self.forward_transform(),spectrogram.abs().square() + self.eps))
        self.replace(envelope_parameters_dB)
        
    def replace_unused(self):
        with torch.no_grad():
            unused_count = self.unused_indices.shape[0]
            used = self.codebooks[self.best_codebook_indices].clone()
            while used.shape[0] < unused_count:
                used = torch.cat((used,used),dim=0)
            self.codebooks[self.unused_indices] *= 0
            self.codebooks[self.unused_indices] += used[range(unused_count)] + 1e-12*torch.randn((unused_count,self.envelope_dimensions),device=self.device).clone()
            # self.codebooks_used[:] = False
            print('Replaced ' + str(unused_count))

