#!/usr/bin/env python
# coding: utf-8



import numpy as np
from scipy import signal




# TDoA (ms)
def lag_finder(y1, y2, sr, maxlag_ms = 500):
    n = len(y1)
    corr = signal.correlate(y2, y1, mode='same') / np.sqrt(signal.correlate(y1, y1, mode='same')[int(n/2)] * signal.correlate   (y2, y2, mode='same')[int(n/2)])

    maxlag = np.min([maxlag_ms/1000, n/sr])

    delay_arr = np.linspace(-0.5*n/sr, 0.5*n/sr, n)
    lag_range = np.abs(delay_arr) <= maxlag
    delay_ix = np.argmax(corr.squeeze()*lag_range.squeeze())
    delay = delay_arr[delay_ix]
    delay_ms = (delay*1000)
    #print('TDoA is ' + delay_ms + ' (ms)')

    #plt.figure()
    #plt.plot(delay_arr, corr)
    #plt.title('Lag: ' + str(np.round(delay, 3)) + ' s')
    #plt.xlabel('Lag')
    #plt.ylabel('Correlation coeff')
    #plt.show()

    return delay_ms

