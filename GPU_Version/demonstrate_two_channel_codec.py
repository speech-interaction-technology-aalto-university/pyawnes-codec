# functional
import numpy as np
import torch 
import singlechannelcodec
import twochannelcodec
import torchaudio
import RIR_Simulate
import os

# dev
import matplotlib.pyplot as plt

# config
import codec_config

# dataset
import corpus
import configparser
path = "./l/sounds/LibriSpeech/dev-clean/"

# test config
#bitrate_list = np.array([8000, 9600, 13200, 16400, 24400, 32000])
bitrate_ch1 = 8000
bitrate_ch2 = 32000
bitrate_single = bitrate_ch1+bitrate_ch2

SNRdB_ch1 = 30
SNR_ch1 = 10**(SNRdB_ch1/20)
SNRdB_ch2 = 10
SNR_ch2 = 10**(SNRdB_ch2/20)



# initialize 
codec_ch1 = singlechannelcodec.single_channel_codec(bits_per_second=bitrate_ch1)
codec_ch2 = singlechannelcodec.single_channel_codec(bits_per_second=bitrate_ch2)
codec_joint = twochannelcodec.two_channel_codec(codec_ch1,codec_ch2)
codec_single = singlechannelcodec.single_channel_codec(bits_per_second=bitrate_single)

#dataset = corpus.Whisper(dtype=codec_config.dtype)
dataset = corpus.LibriSpeech(mode='test',dtype=codec_config.dtype)
params = {'batch_size': 1,
          'shuffle': True,
          'num_workers': 4}

config = configparser.ConfigParser()
config.read('config.ini')
noise_path = config['Noise']['path']
noise_meta = torchaudio.info(noise_path)
resampler = torchaudio.transforms.Resample(orig_freq=noise_meta[0].rate,new_freq=codec_ch1.fs)

data_generator = torch.utils.data.DataLoader(dataset, **params)



for batch in data_generator:
    
    # input data
    original = batch[0]
    break

# /------------------------------------\
# |                                    |
# |           S                        |
# |                                    |
# |                                    |
# |                         Ch2        |
# |                                    |
# |       Ch1                          |
# |                                    |
# \------------------------------------/
room_dim = [5, 3, 2.5]
source_pos = [2, 1, 1.5]
mic_pos1 = [1.5, 1.5, 1.5]
mic_pos2 = [4, 2, 1.5]        
source_ch1 = torch.from_numpy(RIR_Simulate.Simulate_RIR(original.squeeze(), codec_ch1.fs, source_pos, mic_pos1, room_dim)).unsqueeze(1)
source_ch2 = torch.from_numpy(RIR_Simulate.Simulate_RIR(original.squeeze(), codec_ch2.fs, source_pos, mic_pos2, room_dim)).unsqueeze(1)
samples = np.min([len(source_ch1),len(source_ch2),len(original)])
source_ch1 = source_ch1[range(samples)]
source_ch2 = source_ch2[range(samples)]

noiselen = 100+int(np.ceil(samples*noise_meta[0].rate/codec_ch1.fs))
noiseoffset = np.random.randint(noise_meta[0].length/2-noiselen)
noise,noise_fs = torchaudio.load(noise_path,num_frames=noiselen,offset=noiseoffset)
noise = resampler(noise[0,:])
noise = noise[range(samples)].unsqueeze(1)                

noise_scale_ch1 = source_ch1.std()/(SNR_ch1*noise.std())
noise_scale_ch2 = source_ch2.std()/(SNR_ch2*noise.std())
ch1 = source_ch1 + noise*noise_scale_ch1
ch2 = source_ch2 + noise*noise_scale_ch2

noise_energy_ch1 = codec_ch1.tf.transform(noise).square().mean(dim=1,keepdims=True)*(noise_scale_ch1**2)
noise_energy_ch2 = codec_ch2.tf.transform(noise).square().mean(dim=1,keepdims=True)*(noise_scale_ch2**2)

original = original[range(samples)]
    
# two-channel codec
joint_estimate, quantized_ch1, quantized_ch2, Delay_ms = codec_joint.encode(ch1,ch2,noise_energy_ch1,noise_energy_ch2)


# single-channel codec
quantized_single = codec_single.encode(ch2)

joint_estimate /= 1.1*joint_estimate.abs().max()
quantized_single /= 1.1*quantized_single.abs().max()


        
if os.path.isdir('temp') == False: os.system('mkdir temp')
torchaudio.save('temp/orig.wav',original.t().type(torch.float32),corpus.fs)
torchaudio.save('temp/joint_estimate.wav',joint_estimate.t().type(torch.float32),corpus.fs)
torchaudio.save('temp/quantized_single.wav',quantized_single.t().type(torch.float32),corpus.fs)
if 1:
    os.system('ffplay temp/orig.wav -autoexit -nodisp')     
    os.system('ffplay temp/joint_estimate.wav -autoexit -nodisp')    
    os.system('ffplay temp/quantized_single.wav -autoexit -nodisp')    
