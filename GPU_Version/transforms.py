#!/usr/bin/env python

# from https://github.com/nils-werner/mdct/blob/master/mdct/fast/transforms.py
""" Module for calculating DCT type 4 using FFT and pre/post-twiddling
.. warning::
    These core transforms will produce aliasing when used without overlap.
    Please use :py:mod:`mdct` unless you know what this means.
"""

import numpy
#import scipy
import torch
import torch.fft

__all__ = [
    'mdct', 'imdct',
    'mdst', 'imdst',
    'cmdct', 'icmdct',
    'mclt', 'imclt',
]


def split_complex(x):
    """ Split a complex-valued 2D tensor into 3D tensor, where real and imaginary parts
    are in the last two dimensions
    """
    D = len(x.shape)
    x = x.unsqueeze(D)
    return torch.cat((torch.real(x), torch.imag(x)),dim=D)

def merge_complex(x):
    return x[...,0] + 1j*x[...,1]

#def get_real(x):
#    return x[:,0]
#
#def get_imag(x):
#    return x[:,1]

def mdct(x, odd=True):
    """ Calculate modified discrete cosine transform of input signal
    Parameters
    ----------
    X : array_like
        The input signal
    odd : boolean, optional
        Switch to oddly stacked transform. Defaults to :code:`True`.
    Returns
    -------
    out : array_like
        The output signal
    """
    return torch.real(cmdct(x, odd=odd)) * numpy.sqrt(2)


def imdct(X, odd=True):
    """ Calculate inverse modified discrete cosine transform of input signal
    Parameters
    ----------
    X : array_like
        The input signal
    odd : boolean, optional
        Switch to oddly stacked transform. Defaults to :code:`True`.
    Returns
    -------
    out : array_like
        The output signal
    """
    return icmdct(X, odd=odd) * numpy.sqrt(2)


def mdst(x, odd=True):
    """ Calculate modified discrete sine transform of input signal
    Parameters
    ----------
    X : array_like
        The input signal
    odd : boolean, optional
        Switch to oddly stacked transform. Defaults to :code:`True`.
    Returns
    -------
    out : array_like
        The output signal
    """
    return -1 * torch.imag(cmdct(x, odd=odd)) * numpy.sqrt(2)


def imdst(X, odd=True):
    """ Calculate inverse modified discrete sine transform of input signal
    Parameters
    ----------
    X : array_like
        The input signal
    odd : boolean, optional
        Switch to oddly stacked transform. Defaults to :code:`True`.
    Returns
    -------
    out : array_like
        The output signal
    """
    return -1 * icmdct(X * 1j, odd=odd) * numpy.sqrt(2)



def cmdct(x, odd=True):

    device = x.device
    """ Calculate complex MDCT/MCLT of input signal
    Parameters
    ----------
    x : array_like
        The input signal
    odd : boolean, optional
        Switch to oddly stacked transform. Defaults to :code:`True`.
    Returns
    -------
    out : array_like
        The output signal
    """
    N = len(x) // 2
    n0 = (N + 1) / 2
    if odd:
        outlen = N
        pre_twiddle = torch.exp(-1j * torch.tensor(numpy.pi) * torch.arange(N * 2) / (N * 2))
        pre_twiddle = pre_twiddle.to(device)
        offset = 0.5
    else:
        outlen = N + 1
        pre_twiddle = 1.0
        offset = 0.0

    post_twiddle = torch.exp(-1j * torch.tensor(numpy.pi) * n0 * (torch.arange(outlen) + offset) / N).unsqueeze(1)
    post_twiddle = post_twiddle.to(device)

    
    #X = merge_complex(torch.fft(split_complex(x*pre_twiddle),1))[:outlen].unsqueeze(1)
    X = torch.fft.fft(x*pre_twiddle)[:outlen].unsqueeze(1)

    if not odd:
        X[0] *= torch.sqrt(0.5)
        X[-1] *= torch.sqrt(0.5)

    return torch.mul(post_twiddle, X) * numpy.sqrt(1 / N)


def icmdct(X, odd=True):
    """ Calculate inverse complex MDCT/MCLT of input signal
    Parameters
    ----------
    X : array_like
        The input signal
    odd : boolean, optional
        Switch to oddly stacked transform. Defaults to :code:`True`.
    Returns
    -------
    out : array_like
        The output signal
    """
    if not odd and len(X) % 2 == 0:
        raise ValueError(
            "Even inverse CMDCT requires an odd number "
            "of coefficients"
        )

    X = X.detach()

    if odd:
        N = len(X)
        n0 = (N + 1) / 2

        post_twiddle = torch.exp(1j * torch.tensor(numpy.pi) * (torch.arange(N * 2) + n0) / (N * 2)).unsqueeze(1)

        Y = torch.zeros(N * 2, dtype=X.dtype)
        Y[:N] = X
        Y[N:] = -1 * torch.conj(X.flipud())
    else:
        N = len(X) - 1
        n0 = (N + 1) / 2

        post_twiddle = 1.0

        X[0] *= torch.sqrt(2)
        X[-1] *= torch.sqrt(2)

        Y = torch.zeros(N * 2, dtype=X.dtype)
        Y[:N+1] = X
        Y[N+1:] = -1 * torch.conj(X[-2:0:-1])

    pre_twiddle = torch.exp(1j * torch.tensor(numpy.pi) * n0 * torch.arange(N * 2) / N)

    #y = merge_complex(torch.ifft(split_complex(torch.mul(pre_twiddle,Y)),1)).unsqueeze(1)
    y = torch.fft.ifft(torch.mul(pre_twiddle,Y)).unsqueeze(1)
    
    return torch.real(torch.mul( post_twiddle,y)) * numpy.sqrt(N)


mclt = cmdct
imclt = icmdct

