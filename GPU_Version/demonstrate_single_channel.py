# functional
import numpy as np
import torch 
import torchaudio
import singlechannelcodec
import os

# dev
import matplotlib.pyplot as plt

# config
import codec_config

# dataset
import corpus

# initialize 
dataset = corpus.LibriSpeech(mode='test',dtype=codec_config.dtype)
params = {'batch_size': 1,
          'shuffle': True,
          'num_workers': 4}


def psnr(orig,x,perc):
    perc_orig = orig*perc
    perc_x = x*perc
    perc_err = perc_orig - perc_x
    tot_perc_err = perc_err.square().sum(dim=0)
    tot_perc_orig = perc_orig.square().sum(dim=0)
    ix = tot_perc_orig > 0
    snr = tot_perc_orig[ix].div(tot_perc_err[ix])
    return 10*snr.log10().mean()

def np_scale_float_to_int16(x):
    x -= np.mean(x)    
    x *= (2**15-2)/np.max(np.abs(x))
    return x.astype(np.int16)


 

data_generator = torch.utils.data.DataLoader(dataset, **params)
codec1 = singlechannelcodec.single_channel_codec(bits_per_second=16000)

# get random file from LibriSpeech
for batch in data_generator:
    
    # input data
    original = batch[0]
    
    break

# encode
quantized = codec1.encode(original)



# visualize
plt.subplot(211)
plt.plot(original)
plt.subplot(212)
plt.plot(quantized)
plt.show()

plt.plot(codec1.bits_envelope)
plt.show()

plt.subplot(211)
plt.imshow(codec1.spectrogram.abs().log(),aspect='auto',origin='lower')
plt.subplot(212)
plt.imshow(codec1.quantized_spectrogram.abs().log(),aspect='auto',origin='lower')
plt.show()


if os.path.isdir('temp') == False: os.system('mkdir temp')
torchaudio.save('temp/orig.wav',original.t().type(torch.float32),corpus.fs)
torchaudio.save('temp/quantized.wav',quantized.t().type(torch.float32),corpus.fs)
if 1:
    os.system('ffplay temp/orig.wav -autoexit -nodisp')     
    os.system('ffplay temp/quantized.wav -autoexit -nodisp')    



