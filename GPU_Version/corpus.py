#!/usr/bin/env python

#import flac
import torchaudio
import torch
#import flac
#import devtools 
#import numpy as np
import os
import librosa
import codec_config
import configparser

fs = 16000




def torch_zeroextend_to_length(x,N,dim=0):
    ext_N = N-x.shape[dim]
    ext_shape = list(x.shape)
    ext_shape[dim] = ext_N
    return torch.cat((x,torch.zeros(ext_shape,dtype=x.dtype)),dim=dim)


class Corpus(torch.utils.data.Dataset):
    'Corpus as dataset'
    #def __init__(self, path, dtype):
    #    'Initialization'
    #    self.filelist = dirr(path)
    #    self.dtype = dtype

    def __len__(self):
        'Denotes the total number of samples'
        return len(self.filelist)
    
    def filecount(self):
        return len(self.filelist)
        
    def __getitem__(self, index):
        'Generates one sample of data'
        # Select sample
        filename = self.filelist[index]        
        
        # Load data 
        if self.interface == 'librosa':
            x,fs = librosa.core.load(filename) 
            x = torch.tensor(x,dtype=codec_config.dtype).unsqueeze(1)
        elif self.interface == 'torchaudio':
            x, fs = torchaudio.load(filename)
            x = x.t()
        elif self.interface == 'flac':
            fs, x = flac.read(filename) 
        
        return x.type(self.dtype)

class LibriSpeech(Corpus):
    def __init__(self,mode='dev',dtype='float'):
        config = configparser.ConfigParser()
        config.read('config.ini')
        self.config = config['LibriSpeech']
        self.name = "LibriSpeech" 
        path = self.config['path']
        train = self.config['train']
        dev = self.config['dev']
        test = self.config['test']
        self.url_base = self.config['url_base']
        self.url_post = self.config['url_post']
        self.train_url = self.url_base + train + self.url_post
        self.test_url = self.url_base + test + self.url_post
        self.dev_url = self.url_base + dev + self.url_post
        
        if mode=='dev': mode_path = path + dev
        elif mode=='train': mode_path = path + train
        elif mode=='test': mode_path = path + test
        else: 
            raise Exception("Unknown part of LibriSpeech: " + str(mode) )
        self.filelist = self.dirr(mode_path)
        self.dtype = dtype      

        print('Corpus name: ' + self.name + ' Partition: ' + mode)

        self.interface = config['Interface']['AudioLibrary']

        print('Audio Library: ' + self.interface)

    # dir recursive
    def dirr(self,d):

        filelist = list()

        f = os.listdir(d)
        for k in range(len(f)):
            f[k] = d+'/'+f[k]
        while len(f) > 0:
            if f[0].endswith('flac'):
                filelist.append(f[0])
            elif (os.path.isdir(f[0]) == True) and (f[0].endswith('.') == False):
                foo = os.listdir(f[0]+'/')
                for k in range(len(foo)):
                    f.append(f[0]+'/'+foo[k]) 
            del f[0]
            
        return filelist

