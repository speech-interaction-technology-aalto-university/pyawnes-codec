import torch
import numpy as np
import TDoA
from arlpy import bf


class two_channel_codec():
    def __init__(self,single_channel_codec_ch1,single_channel_codec_ch2):
        self.single_channel_codec_ch1 = single_channel_codec_ch1
        self.single_channel_codec_ch2 = single_channel_codec_ch2

    def delay_compensation(self,X1,d,codec):
        
        # MDCT matrix
        proto_MDCT_matrix = codec.tf.inverse_transform(torch.eye(codec.tf.step))
        MDCT_matrix = torch.mul(proto_MDCT_matrix,codec.tf.function)

        M0 = torch.cat((MDCT_matrix,torch.zeros(codec.tf.step,MDCT_matrix.shape[1])))
        M1 = torch.cat((torch.zeros(codec.tf.step,MDCT_matrix.shape[1]),MDCT_matrix))
        Mx = torch.cat((M0,M1),dim=1)

        Md = torch.cat((torch.zeros(d,MDCT_matrix.shape[1]),MDCT_matrix,torch.zeros(codec.tf.step-d,MDCT_matrix.shape[1])))

        C = torch.matmul(Md.t(),Mx)

        X1x = torch.cat((X1[:,0:-1],X1[:,1:]),dim=0)
        X0h = C @ X1x
        return X0h

    def encode(self,ch1,ch2,noise_energy_ch1=0,noise_energy_ch2=0):
        #spectrogram_ch1 = self.single_channel_codec_ch1.tf.transform(ch1)
        #spectrogram_ch2 = self.single_channel_codec_ch2.tf.transform(ch2)

        _ = self.single_channel_codec_ch1.encode(ch1)
        _ = self.single_channel_codec_ch2.encode(ch2)
        quantized_spectrogram_ch1 = self.single_channel_codec_ch1.quantized_spectrogram
        quantized_spectrogram_ch2 = self.single_channel_codec_ch2.quantized_spectrogram
        noise_magnitude_spectrogram_ch1 = self.single_channel_codec_ch1.noise_magnitude_spectrogram
        noise_magnitude_spectrogram_ch2 = self.single_channel_codec_ch2.noise_magnitude_spectrogram
        
        # Brute-force: reconstrct white spectrum to estimate TDoA
        white_ch1 = self.single_channel_codec_ch1.tf.reconstruct_transform(torch.tensor(self.single_channel_codec_ch1.quantized_white_spectrogram))
        white_ch2 = self.single_channel_codec_ch2.tf.reconstruct_transform(torch.tensor(self.single_channel_codec_ch2.quantized_white_spectrogram))                                                                                             
                                                
        Delay_ms = TDoA.lag_finder(white_ch1,white_ch2,self.single_channel_codec_ch1.fs)
        estimated_delay_in_samples = int(Delay_ms*self.single_channel_codec_ch1.fs/1000)
                
        frame_delay = int(np.round(estimated_delay_in_samples / self.single_channel_codec_ch1.tf.step))
        estimated_delay_in_samples -= frame_delay*self.single_channel_codec_ch1.tf.step

        if frame_delay > 0:
            quantized_spectrogram_ch1 = quantized_spectrogram_ch1[:,0:-frame_delay]
            quantized_spectrogram_ch2 = quantized_spectrogram_ch2[:,frame_delay:]
            spectrogram_ch1 = spectrogram_ch1[:,0:-frame_delay]
            spectrogram_ch2 = spectrogram_ch2[:,frame_delay:]
            noise_magnitude_spectrogram_ch1 = noise_magnitude_spectrogram_ch1[:,0:-frame_delay]
            noise_magnitude_spectrogram_ch2 = noise_magnitude_spectrogram_ch2[:,frame_delay:]
        elif frame_delay < 0:
            quantized_spectrogram_ch1 = quantized_spectrogram_ch1[:,-frame_delay:]
            quantized_spectrogram_ch2 = quantized_spectrogram_ch2[:,0:frame_delay]
            spectrogram_ch1 = spectrogram_ch1[:,-frame_delay:]
            spectrogram_ch2 = spectrogram_ch2[:,0:frame_delay]
            noise_magnitude_spectrogram_ch1 = noise_magnitude_spectrogram_ch1[:,-frame_delay:]
            noise_magnitude_spectrogram_ch2 = noise_magnitude_spectrogram_ch2[:,0:frame_delay]

            

        if 1:
            # Brute-force in MDCT domain
            noise_energy_ch1 = noise_magnitude_spectrogram_ch1.square() + noise_energy_ch1
            noise_energy_ch2 = noise_magnitude_spectrogram_ch2.square() + noise_energy_ch2
            if estimated_delay_in_samples > 0:
                synched_quantized_spectrogram_ch2 = self.delay_compensation(quantized_spectrogram_ch2,int(estimated_delay_in_samples),self.single_channel_codec_ch1)
                spectrogram_joint = (quantized_spectrogram_ch1[:,0:-1] + synched_quantized_spectrogram_ch2)/2
                #spectrogram_joint = (quantized_spectrogram_ch1[:,0:-1]*noise_energy_ch2[:,0:-1] + synched_quantized_spectrogram_ch2*noise_energy_ch1[:,0:-1])/(noise_energy_ch1[:,0:-1]+noise_energy_ch2[:,0:-1])

                quantized_ch1 = self.single_channel_codec_ch1.tf.reconstruct_transform(quantized_spectrogram_ch1)
                quantized_ch2 = self.single_channel_codec_ch2.tf.reconstruct_transform(quantized_spectrogram_ch2)
                joint_estimate = self.single_channel_codec_ch1.tf.reconstruct_transform(spectrogram_joint)
            else:
                synched_quantized_spectrogram_ch1 = self.delay_compensation(quantized_spectrogram_ch1,int(-estimated_delay_in_samples),self.single_channel_codec_ch1)
                spectrogram_joint = (quantized_spectrogram_ch2[:,0:-1] + synched_quantized_spectrogram_ch1)/2
                #spectrogram_joint = (quantized_spectrogram_ch2[:,0:-1]*noise_energy_ch1[:,0:-1] + synched_quantized_spectrogram_ch1*noise_energy_ch2[:,0:-1])/(noise_energy_ch2[:,0:-1]+noise_energy_ch1[:,0:-1])

                quantized_ch1 = self.single_channel_codec_ch1.tf.reconstruct_transform(quantized_spectrogram_ch1)
                quantized_ch2 = self.single_channel_codec_ch2.tf.reconstruct_transform(quantized_spectrogram_ch2)
                joint_estimate = self.single_channel_codec_ch1.tf.reconstruct_transform(spectrogram_joint)

            #print([quantized_spectrogram_ch1.abs().max().detach().numpy(), quantized_spectrogram_ch2.abs().max().detach().numpy(), quantized_ch1.abs().max().detach().numpy(), quantized_ch2.abs().max().detach().numpy() ])

        else:
            # Ignorant: delay-and-sum in time-domain
            quantized_ch1 = self.single_channel_codec_ch1.tf.reconstruct_transform(quantized_spectrogram_ch1)
            quantized_ch2 = self.single_channel_codec_ch2.tf.reconstruct_transform(quantized_spectrogram_ch2)

            quantized_channels = np.concatenate((quantized_ch1.numpy(), quantized_ch2.numpy()), axis=1)# timeseries data for multiple sensors (row per sensor)
            quantized_channels = np.transpose(quantized_channels)

            channel_delays = np.array([0,float(Delay_ms)/1000]).reshape(1,2) #sd – steering delays vector [0 delay] (sec)

            joint_estimate = torch.from_numpy(bf.delay_and_sum(quantized_channels, self.single_channel_codec.fs,channel_delays)).t()# delay and sum beamforming

        self.quantized_spectrogram_ch1 = quantized_spectrogram_ch1
        self.quantized_spectrogram_ch2 = quantized_spectrogram_ch2
        self.spectrogram_ch1 = self.single_channel_codec_ch1.spectrogram
        self.spectrogram_ch2 = self.single_channel_codec_ch2.spectrogram
        self.spectrogram_joint = spectrogram_joint
        return joint_estimate, quantized_ch1, quantized_ch2, Delay_ms