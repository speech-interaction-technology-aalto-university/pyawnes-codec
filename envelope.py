#!/usr/bin/env python

import torch
import numpy as np
import os
import codingtools
import configparser



# envelope model
class Envelope():
    def __init__(self,codec_config):
        config = configparser.ConfigParser()
        config.read('codec_config.ini')        
        self.order = int(config['envelope']['order'])

class LinearModel(Envelope):
    
    def __init__(self,codec_config,tf):
        Envelope.__init__(self,codec_config)    
        
        self.eps = codec_config.eps
        
        self.dq = torch.tensor(np.load(codec_config.path_envelope_quantization_step),dtype=codec_config.dtype,requires_grad=False)
        self.envelopemtx = torch.tensor(np.load(codec_config.path_envelope_matrix),dtype=codec_config.dtype,requires_grad=False)
        self.ienvelopemtx = torch.tensor(np.load(codec_config.path_inverse_envelope_matrix),dtype=codec_config.dtype,requires_grad=False)
        if os.path.isfile(codec_config.path_envelope_normalization):
            params = np.load(codec_config.path_envelope_normalization,allow_pickle=True)[()]
            self.var = params['variance'].unsqueeze(1)
            self.mu = params['mean'].unsqueeze(1)
            self.std = self.var**0.5
            self.istd = self.var**-0.5
            #file = np.load(codec_config.path_envelope_normalization)
            #self.istd = torch.tensor(file['arr_0'],dtype=codec_config.dtype,requires_grad=False)
            #self.std = self.istd.pow(-1)
            #self.mu = torch.tensor(file['arr_1'],dtype=codec_config.dtype,requires_grad=False)
            self.ndq = self.dq.mul(self.istd.squeeze())
        else:
            print('Warning: No envelope normalization found.')
            self.istd = 1
            self.std = 1
            self.mu = 0
            self.ndq = self.dq

        
    def extract_parameters(self,spectrum):
        return self.envelopemtx.matmul(spectrum.pow(2)+self.eps)
    
    def reconstruct_power_envelope(self,envelope_parameters):
        return self.ienvelopemtx.matmul(envelope_parameters)

    def reconstruct_smooth_envelope(self,envelope_parameters):
        return self.envelopemtx.t().matmul(envelope_parameters)

    def normalize_envelope_parameters(self, envelope_parameters):
        return (codingtools.power_to_dB(envelope_parameters) - self.mu).mul(self.istd)
    
    def unnormalize_envelope_parameters(self, normalized_envelope_parameters):
        return codingtools.dB_to_power(normalized_envelope_parameters.mul(self.std) + self.mu)
    
    def whiten_spectrum(self,spectrum,power_envelope,biased=False):
        return torch.div(spectrum,self.eps+power_envelope.abs().sqrt())

    def unwhiten_spectrum(self,spectrum,power_envelope,biased=False):
        return torch.mul(spectrum,self.eps+power_envelope.abs().sqrt())
    

class EnvelopeCoder():
    def __init__(self,codec_config,env):
        self.env = env
        self.codec_config = codec_config
        #self.C = torch.tensor(np.load('modeldata/single_gaussian_envelope.npy'),dtype=codec_config.dtype)
        #self.C = torch.tensor(np.load(codec_config.path_single_gaussian_envelope),dtype=codec_config.dtype)
        params = np.load(codec_config.path_envelope_normalization,allow_pickle=True)[()]
        #self.C = params['variance']**-0.5
        self.C =  params['variance']*0 + 1
            
        
    def encode(self,envelope_parameters):
        frames = envelope_parameters.shape[1]        
        env = self.env
        C = self.C.diag()
        codec_config = self.codec_config
        
        Lq,Rq = codingtools.quantization_bin(envelope_parameters,env.ndq.unsqueeze(1).expand(env.order,frames))
        quantized_envelope_parameters = (Lq+Rq)/2
        quantized_envelope = env.reconstruct_power_envelope(env.unnormalize_envelope_parameters(quantized_envelope_parameters))
        #envelope_error = (envelope_power - quantized_envelope).square().mean(dim=0).sqrt()


        var_dq = (env.ndq.pow(2)/12)

        cL = codingtools.normcdf((Lq[0,:]).mul(C[0,0].pow(-.5)))
        cR = codingtools.normcdf((Rq[0,:]).mul(C[0,0].pow(-.5)))
        bits = -(cR-cL+codec_config.eps).log2()

        for k in range(1,env.order):
            P = torch.cat((torch.eye(k,dtype=codec_config.dtype),torch.zeros(k,env.order-k,dtype=codec_config.dtype)),dim=1)    
            PC = torch.matmul(P,C)
            PCP = torch.matmul(PC,P.t())
            Cqq = var_dq[0:k].diag()
            B = torch.matmul(PC.t(),(PCP + Cqq).inverse())
            xhat = torch.matmul(B,torch.matmul(P,quantized_envelope_parameters))
            Cee = C - torch.matmul(B,PC)

            iCee = Cee[k,k].pow(-.5)

            cL = codingtools.normcdf((Lq[k,:]-xhat[k,:]).mul(iCee))
            cR = codingtools.normcdf((Rq[k,:]-xhat[k,:]).mul(iCee))    
            bits += -(cR-cL+codec_config.eps).log2()

    
        return bits, quantized_envelope_parameters