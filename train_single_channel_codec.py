# functional
import windowing
import numpy as np
import torch 
import configparser
import training_models
import os
import codingtools
import envelope
import scipy
#import singlechannelcodec


# To train all even if models already exist, set to 'True'
force_training = False
force_training_spectral_pdf = True

# dev
import matplotlib.pyplot as plt

# config
import codec_config

# dataset
import corpus

# initialize 
dataset = corpus.LibriSpeech(dtype=codec_config.dtype,mode='train')
params = {'batch_size': 1,
          'shuffle': True,
          'num_workers': 1}
data_generator = torch.utils.data.DataLoader(dataset, **params)          


# codec config
config = configparser.ConfigParser()
config.read('codec_config.ini')
fs = int(config['sampling']['frequency'])
window_length_ms = int(config['window']['length_ms'])
window_step_ms = int(config['window']['step_ms'])
tf = windowing.mdct(window_length_ms,window_step_ms,fs,codec_config.dtype)
envelope_order = int(config['envelope']['order'])



if force_training | (os.path.isfile(codec_config.path_envelope_matrix) == False):
    print('Training of envelope model and spectral pdf')

    # training setup
    spectrum_length = tf.step
    spectrogram_model = training_models.LogisticMixture(spectrum_length,5,codec_config.dtype)
    envelope_model = training_models.EnvelopeModel(spectrum_length, envelope_order, codec_config.dtype, eps=codec_config.eps)
    
    
    # train
    learning_rate = 1e-2
    optimizer = torch.optim.Adam(list(spectrogram_model.parameters()) + list(envelope_model.parameters()), lr=learning_rate)
    
    alpha = 0.01
    batchcounter = 0
    for batch in data_generator:
    
        
        spectrogram = tf.transform(batch[0])
    
        _, magnitude_envelope, negloglike_envelope = envelope_model.forward(spectrogram)
        negloglike_spectrogram = spectrogram_model(spectrogram.t(), magnitude_envelope.t())
    
        loss = negloglike_envelope + negloglike_spectrogram
        
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()
    
        if 'loss_accumulator' in locals():
            loss_accumulator = (1-alpha)*loss_accumulator + alpha*loss
        else:
            loss_accumulator = loss
    
        batchcounter += 1
        if batchcounter % 100 == 0:
            print('Batch ' + str(batchcounter) + ', Loss: ' + str(loss_accumulator.item()))
            plt.subplot(311)
            plt.plot(envelope_model.forward_transform().t().detach().numpy())
            plt.subplot(312)
            plt.plot(envelope_model.backward_transform().detach().numpy())
            plt.subplot(313)
            plt.plot(envelope_model.quantization_step.detach().numpy())
            plt.show()
            frameix=30
            plt.plot(20*spectrogram[:,frameix].abs().log10().detach().numpy())
            plt.plot(20*magnitude_envelope[:,frameix].abs().log10().detach().numpy())
            plt.show()
        if batchcounter == 1000:
            learning_rate = 1e-3
            optimizer = torch.optim.Adam(list(spectrogram_model.parameters()) + list(envelope_model.parameters()), lr=learning_rate)
        if batchcounter == 10000:
            learning_rate = 1e-4
            optimizer = torch.optim.Adam(list(spectrogram_model.parameters()) + list(envelope_model.parameters()), lr=learning_rate)
            np.save(codec_config.path_envelope_matrix,envelope_model.forward_transform().detach().numpy())
            np.save(codec_config.path_inverse_envelope_matrix,envelope_model.backward_transform().detach().numpy())
            np.save(codec_config.path_envelope_quantization_step, envelope_model.quantization_step.squeeze().detach().numpy())        
            
            prop, A, mu = spectrogram_model.extract_parameters()        
            spectral_pdf = {
                "proportion": prop.squeeze().detach().numpy(),
                "A": A.squeeze().detach().numpy(),
                "mu": mu.squeeze().detach().numpy() 
                }
            np.save(codec_config.path_spectral_pdf,spectral_pdf)
    
    
    np.save(codec_config.path_envelope_matrix,envelope_model.forward_transform().detach().numpy())
    np.save(codec_config.path_inverse_envelope_matrix,envelope_model.backward_transform().detach().numpy())
    np.save(codec_config.path_envelope_quantization_step, envelope_model.quantization_step.squeeze().detach().numpy())
    prop, A, mu = spectrogram_model.extract_parameters()        
    spectral_pdf = {
        "proportion": prop.squeeze().detach().numpy(),
        "A": A.squeeze().detach().numpy(),
        "mu": mu.squeeze().detach().numpy() 
        }
    np.save(codec_config.path_spectral_pdf,spectral_pdf)



if force_training | (os.path.isfile(codec_config.path_envelope_normalization) == False):
    print('Training of entropy model and normalization for envelope (mean and variance)')
    
    if 0:
        # training setup
        spectrum_length = tf.step
        envelope_model = training_models.EnvelopeModel(spectrum_length, envelope_order, codec_config.dtype, eps=codec_config.eps)
        
    
        envelope_parameters_dB_mean = torch.zeros(envelope_order,dtype=codec_config.dtype)    
        envelope_parameters_dB_squared = torch.zeros(envelope_order,dtype=codec_config.dtype)    
        
        
        framecounter = 0
        batchcounter = 0
        for batch in data_generator:
        
            
            spectrogram = tf.transform(batch[0])
            frames = spectrogram.shape[1]
            
            envelope_parameters_dB, _, _ = envelope_model.forward(spectrogram)
            
            envelope_parameters_dB_mean += envelope_parameters_dB.sum(dim=1).detach()
            envelope_parameters_dB_squared += envelope_parameters_dB.square().sum(dim=1).detach()
            framecounter += frames
            
            batchcounter += 1
            if batchcounter % 100 == 0:
                print('Batch ' + str(batchcounter))
        
        envelope_parameters_dB_mean /= framecounter
        envelope_parameters_dB_squared /= framecounter        
    
        envelope_normalization = {
            "mean": envelope_parameters_dB_mean,
            "variance": envelope_parameters_dB_squared - envelope_parameters_dB_mean.square()
            }
        
        np.save(codec_config.path_envelope_normalization,envelope_normalization)
        
    else:
        
        # training setup
        spectrum_length = tf.step      
        env = envelope.LinearModel(codec_config,tf)
    
        envelope_parameters_dB_mean = torch.zeros(envelope_order,dtype=codec_config.dtype)    
        envelope_parameters_dB_var = torch.zeros(envelope_order,dtype=codec_config.dtype)    
        
        
        framecounter = 0
        batchcounter = 0
        for batch in data_generator:
        
            
            spectrogram = tf.transform(batch[0])
            frames = spectrogram.shape[1]
            
            envelope_parameters = env.extract_parameters(spectrogram)
            envelope_parameters_dB = codingtools.power_to_dB(envelope_parameters)
            
            envelope_parameters_dB_mean += envelope_parameters_dB.mean(dim=1).detach()
            envelope_parameters_dB_var += envelope_parameters_dB.var(dim=1).detach()
            framecounter += frames
            
            batchcounter += 1
            if batchcounter % 100 == 0:
                print('Batch ' + str(batchcounter))
        
        envelope_parameters_dB_mean /= dataset.filecount()
        envelope_parameters_dB_var /= dataset.filecount()
    
        envelope_normalization = {
            "mean": envelope_parameters_dB_mean,
            "variance": envelope_parameters_dB_var
            }
        
        np.save(codec_config.path_envelope_normalization,envelope_normalization)
        

if force_training | force_training_spectral_pdf | (os.path.isfile(codec_config.path_spectral_bitrate_bias) == False):
    print('Training of entropy bias for spectral components (logistic mixture model)')
    
    # initialize codec
    env = envelope.LinearModel(codec_config,tf)
        
    spectral_pdf = np.load(codec_config.path_spectral_pdf,allow_pickle=True)[()]
    
    A = torch.tensor(spectral_pdf['A'],dtype=codec_config.dtype)
    mu = torch.tensor(spectral_pdf['mu'],dtype=codec_config.dtype)
    prop = torch.tensor(spectral_pdf['proportion'],dtype=codec_config.dtype)
    prop /= (prop.sum(dim=1,keepdims=True))
    
    Anp = np.abs(spectral_pdf['A'])
    iAnp = 1/Anp
    munp = spectral_pdf['mu']
    propnp = spectral_pdf['proportion']
    propnp = np.divide(propnp, np.sum(propnp,axis=1,keepdims=True))
    
        
    speclen = A.shape[0]
    spectral_mixture_models = spectral_pdf['A'].shape[1]
    
    Q = 2**-8
    bits_Q = -np.log2(Q)
    
    bits = torch.zeros(speclen,dtype=codec_config.dtype)
    framecnt = 0
    batchcnt = 0
    batchcnt1pct = int(dataset.filecount()/100)
    
    for batch in data_generator:
        spectrogram = tf.transform(batch[0])
        batchcnt += 1
        frames = spectrogram.shape[1]
    
        envelope_parameters = env.normalize_envelope_parameters(env.extract_parameters(spectrogram))
    
        quantized_envelope_parameters = codingtools.quantizer(envelope_parameters,env.ndq.unsqueeze(1).expand(env.order,frames))
        quantized_power_envelope = env.reconstruct_power_envelope(env.unnormalize_envelope_parameters(quantized_envelope_parameters))
    
        white_spectrogram = env.whiten_spectrum(spectrogram,quantized_power_envelope)
        
        framecnt += frames
        if 0:
            dither = torch.rand(frames,dtype=codec_config.dtype)
            
            for k in range(speclen):
                _,bk = codingtools.quantize_lmm(white_spectrogram[k,:],Q,mu[k,:],A[k,:],prop[k,:])
                bits[k] += bk.sum()
        else:
            #dither = torch.rand(speclen,frames,dtype=codec_config.dtype)
            for freqix in range(speclen-1,-1,-1):              
                
                L = (((white_spectrogram[freqix,:]/Q ).floor() )*Q).detach().numpy()
                R = L + Q
    
                Lw = Anp[freqix,:]*(np.reshape(L,[frames,1])-munp[freqix,:])
                Rw = Anp[freqix,:]*(np.reshape(R,[frames,1])-munp[freqix,:])
                pw = scipy.special.expit(Rw) - scipy.special.expit(Lw)
                #pwh = pwh/(np.sum(pwh,axis=1,keepdims=True)+1e-12)
                p = np.sum(pwh*propnp[freqix,:],axis=1)

                bk = -np.log2(p+1e-12)                
                bits[freqix] += bk.sum()
            
    
        if batchcnt % batchcnt1pct == 0:
            plt.plot((bits/framecnt - bits_Q).detach().numpy())
            plt.title(str(int(batchcnt / batchcnt1pct)) + '%')
            plt.show()
        
    bit_bias = (bits/framecnt - bits_Q)
    np.save(codec_config.path_spectral_bitrate_bias,bit_bias.detach().numpy())