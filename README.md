# The PyAWNeS-Codec

A speech and audio codec for ad-hoc, acoustic wireless networks of sensors, implemented with Python and Pytorch.

## Preliminaries

Pronounciation: "Pyjanes"-codec (rhymes with Pyjamas)

NOTE: Storage location is specified in file 'config.ini', whose format is

    [LibriSpeech]
    path = /l/sounds/LibriSpeech/
    dev = dev-clean/
    train = train-clean-100/
    test = test-clean/

    [Interface]
    AudioLibrary = torchaudio

Required packages:

- pytorch==1.8 with "conda install pytorch torchvision torchaudio -c pytorch-nightly"
- numpy==1.19.2 with "conda install numpy"
- scipy==1.5.2 with "conda install scipy"
- wget==3.2 with "pip install wget"
- torchaudio==0.8.0a0+ba6d67b with "conda install -c pytorch-nightly torchaudio"
- if you have trouble with torchaudio, try librosa with "conda install -c conda-forge librosa"
- sounddevice==0.4.1 with "conda install -c conda-forge python-sounddevice"
- soundfile=0.10.2 with "sudo apt-get install libsndfile1" and "conda install python-soundfile"? or "pip install soundfile"
- arlpy==1.7.0 with "conda install -c conda-forge arlpy" (should become obsolete soon)
- if sounddevice does not work, then you can use ffmpeg instead "sudo apt install ffmpeg"

## Codec presentation

### Idea

- Many microphones give better quality signal and user-interface than a single device.
  - Better spatial sampling of the acoustic space.
  - Release the device-centric focus for the benefit of a user-centric focus.
- We already have many microphones = no need for dedicated hardware
  - Sustainability and cost reduction through efficient use of hardware.
- *Constraint*: Single channel must be similar in *quality* to state-of-the-art single channel codecs
  - *Problem*: Cannot sell a lower-quality codec
  - *Problem*: Makes it difficult to use conventional distributed coding, because lacking support for envelope modelling
  - *Consequence*: Have to use conventional single channel codec as starting point
  - *Consequence*: Introduce new features to support distributed ad-hoc sensors

### Overall structure

<img src="illustrations/structure_overall.png" width=400 align="right" style="background-color:#ffffff;" />

- The codec is built on a set of independent encoders and decoders, whose output is merged in post-processing.
- Thus devices do not have to know anything about each other *and* there is no overhead from communicating with other devices.
  - Distributed source coding without inter-node communication is possible, *but*  not aware of distributed source coding methods which can be combined with perceptual modelling at low bitrates.
- The benefit is simplicity, but the cost is that we cannot guarantee optimality.
- The target signal is not synchronized across channels;
  - Devices might not be synchornized, but that could be potentially fixed
  - The length of the acoustic path varies, such that the arrive at different times to the sensors - must be compensated!

<img src="illustrations/structure_merge.png" width=450 align="right" style="background-color:#ffffff;"/>

- Current solution: Channels are merged with the simplest possible approach;
  - Time-difference of arrival (TDoA) estimation and
  - Delay compensation (delay-and-sum)

### Single-channel codec

<img src="illustrations/structure_single_codec.png" width=650 align="right" style="background-color:#ffffff;"/>

- Time-frequency transform with MDCT
  - Similar to STFT, but with orthonormal windows and real-valued transform (DCT)
  - Low-delay windows (30ms length and 20ms step)
- Envelope model starting point is energies of 500 Hz frequency bands (scale factor bands)
  - Used to normalize (whiten) the spectrum -> afterwards, spectrum is zero mean and unit variance
  - Model is trained to get best whitening
  - Envelope parameters are encoded with entropy coder to minimize bitrate
- Perceptual model is derived from quantized envelope
  - Same information is available at decoder
  - Based on same model as AMR-WB and EVS
    - The complexity is obscene, but it gives good quality. To be fixed later.
- Normalized spectrum is then perceptually encoded
  - Normalized perceptual envelope defines bitrate of each component
  - Bitrate defines quantization step size
  - Quantization bins are offset to get dithering (see below)
  - Encoded with entropy coder to minimize bitrate
- Dithering of spectrum is used to
  - Avoid bias to zero, which causes a loss of energy at high frequencies and a muffled sound
  - Get independent information from independent sensors for higher accuracy
- Decoder mirrors the encoder
  - Perceptual model is needed to decode the spectrum, so the envelope has to be decoded first

### Envelope model

<img src="illustrations/figEnv-1.png" align="right" width=400  style="background-color:#ffffff;"/>

- Starting point is energy bands of 500 Hz width (scale factor bands)
- Quantization accuracy adjusted to get an average envelope error of 1dB.
- Entropy coding with a Gaussian model
  - Encode components one by one
  - Predict mean and variance of current component from previous components.
  - More efficient than vector quantization, likely because convergence of vector quantizers is complicated and because high-bitrate quantizers must be approximated by a sequence of low-complexity quantizers.

<img src="illustrations/figEnvSpec-1.png" width=400  align="right" style="background-color:#ffffff;"/>

- Current perceptual envelope is derived from 3GPP EVS
  - Based on smoothed LPC model and low-pass filter (spectral tilt)
  - LPC calculated from quantized power envelope converted to time-domain
  - Will be changed soon (hopefully) to something based on frequency domain operations for lower computational complexity.

### Spectral coding

- Entropy coder is based on logistic mixture models.
  - Each spectral component is separately encoded.
  - Probability distributions of components modelled by a logistic mixture model.

### Post-filtering extension (planned)

- The basic idea is to have a simple codec but then use a range of post-filters to enhance quality at the decoder.
- Post-filters are applied sequentially.
- Each module takes, as input and output, the mean and variance of the estimated spectrum. This is thus a "variational" approach, in the sense that each module should get a better estimate of the mean and reduce variance of the estimate.


<img src="illustrations/postfilter-1.png" align="center" width=950  style="background-color:#ffffff;"/>
