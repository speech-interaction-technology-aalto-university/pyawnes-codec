import torch
import numpy as np
import codec_config
import codingtools

class LogisticMixture(torch.nn.Module):
    def __init__(self, input_dimensions, mixture_models, dtype):
        """
        Create model
        """
        super(LogisticMixture, self).__init__()
        self.input_dimensions = input_dimensions
        self.mixture_models = mixture_models
        self.A = torch.nn.Parameter(torch.rand(input_dimensions,mixture_models,dtype=dtype))
        self.mu = torch.nn.Parameter(torch.randn(input_dimensions,mixture_models,dtype=dtype))
        self.prop0 = torch.nn.Parameter(torch.rand(input_dimensions,mixture_models,dtype=dtype))

    def forward(self, x, scaling):
        """
        Forward pass
        """
        # shape (frames,input_dimensions,mixture_models)
        frames = x.shape[0]
        thisshape = [frames,self.input_dimensions,self.mixture_models]
        invscaling = (scaling.abs() + 1e-12).pow(-1)

        # Normalize component proportion to unity
        prop = self.prop0.abs() # must be positive
        prop /= prop.sum(dim=-1,keepdims=True) + 1e-12 # normalize

        # Normalize component scaling to unit variance
        component_variance = self.A.pow(-2)*(np.pi**2)/3
        joint_mean = (prop*self.mu).sum(dim=-1,keepdims=True)
        joint_variance = (prop*(component_variance + self.mu**2)).sum(dim=-1,keepdims=True) - joint_mean**2
        A = self.A*((joint_variance*3/(np.pi**2)).sqrt().expand([self.input_dimensions,self.mixture_models]))

        # normalize to zero mean and unit variance
        xx = (x*invscaling).unsqueeze(-1).expand(thisshape) # expand with mixture dimensions
        xw = (xx-self.mu.unsqueeze(0).expand(thisshape))*A.unsqueeze(0).expand(thisshape) # normalize mean and variance

        # component probabilities
        cx = torch.sigmoid(xw)
        fx_component = (cx*(1-cx)*A.abs().unsqueeze(0).expand(thisshape))*invscaling.unsqueeze(-1).expand(thisshape)

        # joint probability
        fx = ( fx_component*prop.unsqueeze(0).expand(thisshape) ). sum(dim=-1) + 1e-12

        # negative log likelihood averaged frames
        return -fx.log().sum()/frames
    
    def extract_parameters(self):
        # Normalize component proportion to unity
        prop = self.prop0.abs() # must be positive
        prop /= prop.sum(dim=-1,keepdims=True) + 1e-12 # normalize

        # Normalize component scaling to unit variance
        component_variance = self.A.pow(-2)*(np.pi**2)/3
        joint_mean = (prop*self.mu).sum(dim=-1,keepdims=True)
        joint_variance = (prop*(component_variance + self.mu**2)).sum(dim=-1,keepdims=True) - joint_mean**2
        A = self.A*((joint_variance*3/(np.pi**2)).sqrt().expand([self.input_dimensions,self.mixture_models]))
      
        return prop, A, self.mu

class EnvelopeModel(torch.nn.Module):
    def __init__(self, input_dimensions, envelope_dimensions, dtype, eps=1e-12):
        """
        Create model
        """
        super(EnvelopeModel, self).__init__()
        self.input_dimensions = input_dimensions
        self.envelope_dimensions = envelope_dimensions
        self.eps = eps
        
        envelope0 = torch.zeros(envelope_dimensions,input_dimensions,dtype=dtype)
        envbandwidth = int(input_dimensions/envelope_dimensions)
        for k in range(envelope_dimensions-1):
            envelope0[k,range(k*envbandwidth,(k+1)*envbandwidth)]=1
        envelope0[envelope_dimensions-1,(envelope_dimensions-1)*envbandwidth:]=1

        self.forward_transform_matrix = torch.nn.Parameter(envelope0)
        self.backward_transform_matrix = torch.nn.Parameter(torch.pinverse(envelope0).clone().detach())
        self.quantization_step = torch.nn.Parameter(torch.ones(envelope_dimensions,1,dtype=codec_config.dtype))
        #self.normalization = torch.nn.Parameter(torch.ones(envelope_dimensions,1,dtype=codec_config.dtype))

    def forward_transform(self):
        forward_transform = self.forward_transform_matrix.abs() # non-negative
        forward_transform = forward_transform * (forward_transform.sum(dim=1,keepdims=True) + self.eps).pow(-1).expand([self.envelope_dimensions,self.input_dimensions]) # normalize
        return forward_transform

    def backward_transform(self):
        backward_transform = self.backward_transform_matrix.abs() # non-negative        
        #backward_transform = backward_transform * (backward_transform.sum(dim=1,keepdims=True) + self.eps).pow(-1).expand([self.input_dimensions,self.envelope_dimensions]) # normalize
        #transform_gain = (self.forward_transform() * backward_transform.t()).sum(dim=0,keepdims=True)
        #backward_transform = backward_transform * transform_gain.pow(-1).expand([self.envelope_dimensions,self.input_dimensions]).t()
        return backward_transform

    def forward(self, spectrogram):
        # spectrogram shape (input_dimensions,frames)
        frames = spectrogram.shape[1]        

        # apply transform, output shape (envorder,frames), parameters in decibel
        envelope_parameters_dB = codingtools.power_to_dB(torch.matmul(self.forward_transform(),spectrogram.abs().square() + self.eps))
        
        # quantize
        quantization_noise = self.quantization_step.expand([self.envelope_dimensions,frames])*(torch.rand(self.envelope_dimensions,frames)-0.5)
        quantized_envelope_parameters_dB = envelope_parameters_dB + quantization_noise
        
        # backward transform, output shape (frames, input_dimensions)
        quantized_envelope_power = torch.matmul(self.backward_transform(),codingtools.dB_to_power(quantized_envelope_parameters_dB))
        quantized_envelope_magnitude = quantized_envelope_power.sqrt()

        # whiten/normalize spectrum (obsolete)
        #white_spectrogram = spectrogram*(quantized_envelope_power.pow(-2))

        # negative log2 likelihood is locally relative to the bitrate required to encode envelope
        loss = -((self.quantization_step.abs() + self.eps).log2().sum())

        return envelope_parameters_dB, quantized_envelope_magnitude, loss



