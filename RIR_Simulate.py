import numpy as np
import pyroomacoustics as pra

def Simulate_RIR(in_sig, fs, source_pos, mic_pos, room_dim, rt_60 = 0.3, ray_tracing = False):
    """
    Simulate room impulse response for one source and one microphone in a specific "shoe box" shaped room.
    Params:
        in_sig: input signal
        fs: sampling frequency
        source_pos: array [x,y,z] in meters. Position of the source.
        mic_pos: array [x,y,z] in meters. Position of the microphone.
        room_dim: array [x,y,z] in meters. Dimensions of the room.
        - Corner [0,0,0] of the room is the reference point for the source and microphone position.
        rt_60: Reverberation time in seconds.
        ray_tracing: Enable ray tracing for room simulation. (From personal experience, results are too reverberant)
    Output:
        out_sig: Output signal.
    """


    # Geometry of the room and location of source and microphone
    room_dim = np.array(room_dim)
    source_pos = np.array(source_pos)
    mic_pos = np.array(mic_pos)

    # We invert Sabine's formula to obtain the parameters for the ISM simulator
    e_absorption, max_order = pra.inverse_sabine(rt_60, room_dim)

    # Create the room
    room = pra.ShoeBox(
        room_dim, fs=fs, materials=pra.Material(e_absorption), max_order=max_order, ray_tracing = ray_tracing
    )
    if ray_tracing:
        room.set_ray_tracing()

    room.add_microphone(mic_pos, fs=fs)
    room.add_source(source_pos, signal=in_sig)

    room.simulate()
    out_sig = room.mic_array.signals[0,:]

    #To check the actual simulated rir and the value of the simulated rt60 from source to microphone, remove following comments
    #room.compute_rir()
    #rt60 = room.measure_rt60()

    return out_sig